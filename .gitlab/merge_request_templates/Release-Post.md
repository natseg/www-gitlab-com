**Review Apps** link:  https://release-x-y.about.gitlab.com/YYYY/MM/22/gitlab-x-y-released/

**Intro** link:  https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/source/posts/YYYY-MM-22-gitlab-x-y-released.html.md  
**Items** link:  https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/data/release_posts/YYYY_MM_22_gitlab_x_y_released.yml  
**Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-x-y/source/images/x_y  

**Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/data/features.yml

Release post :rocket:

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/

### General Contributions

General contributions from the team.

Due date: MM/DD (6th working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release 
- [ ] Add milestone 
- [ ] Intro
- [ ] MVP
- [ ] Add cover image `image_title` (compressed)
- [ ] Upgrade barometer
- [ ] Top feature
- [ ] Performance improvements
- [ ] Omnibus improvements
- [ ] GitLab Runner changes
- [ ] Deprecations
- [ ] Documentation links
- [ ] Add features to `data/features.yml`
- [ ] Authorship (author's data)

#### Features

PMs: please check your box only when **all** your features and deprecations were
added with completion (documentation links, images, etc). Pls don't check if
there are still things missing.

- [ ] Fabio - CI/CD
- [ ] Victor - Discussion
- [ ] Mark - I2P
- [ ] Mike - Platform
- [ ] Joshua- Monitoring
- [ ] Nick - Geo
- [ ] Job - Edge

Tip: make your own checklist:

- Primary features
- Improvements (secondary features)
- Deprecations
- Documentation links
- Illustrations
- Update `features.yml`

### Review

Ideally, complete the review until the 4th working day before the 22nd,
so that the 3rd and the 2nd working day before the release
could be left for fixes and small improvements.

#### Content review (PMs)

Due date: MM/DD (2nd working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] General review (PM)
  - [ ] Check Features' names
  - [ ] Check Features' availability (CE, EES, EEP)
  - [ ] Check Documentation links
  - [ ] Update `data/promo.yml`
  - [ ] Check all images' size - compress whenever > 300KB
  - [ ] Meaningful links (SEO)
  - [ ] Check links to product/feature webpages
- [ ] Copyedit (Rebecca or Marcia)
  - [ ] Title
  - [ ] Description
  - [ ] Grammar, spelling, clearness (body)
- [ ] Marketing review (PMM, CMM)
- [ ] Final review (Job)

#### Structural Check

Due date: MM/DD (2nd working day before the 22nd)

- 1. Structural check
- [ ] Label MR: ~"blog post" ~release ~review-structure
- [ ] Check frontmatter (entries, syntax)
- [ ] Check `image_title` and `twitter_image`
- [ ] Check image shadow `{:.shadow}`
- [ ] Check images' `ALT` text
- [ ] Videos/iframes wrapped in `<figure>` tags (responsiveness)
- [ ] Add/check `<!-- more -->` separator
- [ ] Add/check cover img reference (at the end of the post)
- [ ] Columns (content balance between the columns)
- [ ] Badges consistency (applied to all headings)
- [ ] Remove any remaining instructions
- [ ] Remove HTML comments
- [ ] Run deadlink checker
- [ ] Update release template with any changes (if necessary)
