---
layout: markdown_page
title: "GitLab Summits"
---

- TOC
{:toc}

## What, where, and when
{: #what-when-where}

We try to get together every 9 months or so to get face-time, build community,
and get some work done! Since our team is scattered all over the globe, we try to
plan a different location for each Summit.

## Who
{: #who}

All [GitLab Team](/team/), their significant others, and the [Core Team](/core-team/).

## Previous Summits
{: #previous-summits}

### Summit in Cancun, Mexico

In January 2017 we met in Cancun, Mexico, where roughly 150 team members and 50
significant others flew in from 35 different countries.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XDfTj8iv9qw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


### Summit in Austin, TX, the USA

In May 2016, our team of 85 met up in Austin, TX to see if they were (still) as
awesome as seen on Google Hangout.

Here's some footage our team put together to show how much fun we had.

<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/175270564" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


### Summit in Amsterdam, the Netherlands

Here are some impressions from our [first Summit](https://about.gitlab.com/2015/11/30/gitlab-summit-2015/) in October 2015.

<br>


## General information
{: #general-info}

Each Summit is intended to profoundly promote collaboration and understanding
between GitLab contributors. Spouses are welcome to attend, but their presence
should not distract team members from engaging with their peers and actively
participating in Summit activities with the team.

* Significant Others (SOs for short) are very welcome, one per team member
* You are responsible for the SO you invite
* SOs should do their best to get to know many people
* SOs are welcome at all presentations, events, and meetings that are open to
all team members
* If you're having a meal with your SO, pick a table with more than two seats
so you can invite others to join you

### Having your children join you

Children are strongly discouraged from attending. We have observed from past Summits
that contributors who have chosen to bring children spend significantly less time
collaborating with GitLab coworkers. Accordingly, we are requesting that if you
must travel with family members you fully engage in as many group activities as
you are physically able, invite team members to join you during meals, attend all
company meetings, and recognize that the Summit is an investment in the continued
growth of the organization.

{:.alert .alert-info}

The goal of our Summits is to get to know each other outside of work.
To help get this kickstarted, here are a few things to keep in mind.
* Always wear your nametag to all events on all days.
* Try to join different people every time we sit down for a meal
* Try to form a personal bond with different team members of other teams 
* The summit is great for informal meetings and brainstorming, like  User Generated Content discussions. People already know their team, so try to make UGC sessions cross functional.
* Don't plan (extra) formal meetings with your own team at the summit, we already do these when we're at home and remote 
* Reduce regular the meetings and 1-1's with your team to a minimum 


### Health and safety
{: #health-and-safety}

* Use fistbumps instead of handshaking to avoid getting sick
* Remember our [values](https://about.gitlab.com/handbook/values/) and specifically permission to play behavior (bottom of the values page)
* Be respectful of other hotel guests (e.g. don't talk on your floor when returning
to your room & keep your volume down at restaurants/bars)

### Important logistical for every Summit
{: #important-basics}

* Ensure there is great wifi -- we need more serious tech for the main room (5GHz, multiple points)
* Ensure there are large meeting rooms for team members to join work hours and presentations
* Label your charger with your name for easy return to owner in case you lose it

### Presentations
{: #presentations}

The following should be assigned and/or arranged a month before the Summit:

* All interviews, presentations and content production
* Who will be presenting and when & where they will be presenting
* Projectors, wireless (non-handheld) microphones, and any other (audio) needs
* Recording equipment such as stage cam, audience cam, presentation feed etc.
* An audio feed that goes directly from microphone into the recording
* A program and manager for live streaming
* The blog text for the presentation, including relevant materials shared after
the presentation, as well as approval and a plan to publish the blog 24 hours after the presentation is given

#### User Generated Content sessions
User Generated Content sessions are short ~50 minute discussions on a topic that is important to you. 
Topics can be on business, markets, investing, problems, opportunities, life, parenting, etc. Whatever is interesting to our team or the community.
Here's an overview of how this works during our summits.
* We request everyone to [send in topics](https://docs.google.com/spreadsheets/d/15irgt0kOlCNzltsBzWRbJoQzLFjOlOYKgREQ_PRGiuQ/edit#gid=0) to discuss during the sessions at the summit a few weeks out.
    * If you're suggesting a topic, we ask you to be the topic leader. This means the following: 
    * You start the session with a short 3 minute introduction or presentation. 
    * During the discussion you lead the conversation, meaning keeping it on topic, making sure everyone is heard and asking relevant questions. 
    * It could be that we'll have people co-lead a session.
* After all topics are received, we send a [survey](https://docs.google.com/forms/d/e/1FAIpQLScvxsf00cShgj9KIjgIx8Jj60uVFhE_WbtYbHkoa3d2-H7IxQ/viewform) to all attendees asking them to vote on the topics most interesting to them.
* Once we've received all votes, after the deadline, we select the topics for the sessions.
* We schedule 2, 4 hour blocks on separate days to plan the sessions. 
* Within each 4 hour block we schedule 4 sessions (if needed with multiple topics in different rooms/locations) with a short break of ~10 minutes in between for a quick drink/snack or bathroom break.
* TBD Within each session we schedule multiple topics.
* TBD Every session will have a short overview with the topic, leader and more info.
* 5 minutes before the end of each session we'll announce to wrap it up for the switch to another topic.
