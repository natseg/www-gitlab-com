---
layout: job_page
title: "Chief Operating Officer (COO)"
---

DRAFT

The chief operating officer (COO) is responsible for using data to make GitLab's customers more effective.

## Vision

1. Software is eating the world.
1. Every company needs to become an effective software company to survive.
1. Effective software companies have a short time to value.
1. GitLab is the only integrated product that can help you reduce time to value.
1. This will be a long journey and we will partner with our customers to achieve it.
1. Together devise a plan to improve the time to value and we provide content and services.
1. Organizes around the world share the best software development practices in GitLab.
1. GitLab partners with every large organization to reduce time to value.

## Customer perspective

Right now every large enterprise is suffering from a lack of consistency:

1. Not using the same tools
1. Not using the same integrations
1. Not using the same configuration
1. Not using the same work processes
1. Not being judged on the same metrics

And they have processes the block reducing time to value, for example:

1. Security reviews that are blocking until approved
1. Infrastructure that has to be provisioned
1. Fixed release windows
1. Production that needs to approve releases
1. SOX compliance sign offs that need to happen
1. QA cycles
1. Separate QA teams
1. Separate build teams

## Solution

Use data to communicate the same message to the customer throughout all communication from GitLab as a company and product:

- Customer Success (SAs game plans)
- Sales (auto generated presentations)
- Support (intake)
- Marketing (case studies, ROI calculator, web pages, whitepapers, analysts)
- DevRel (meetups, blog posts
- Product start (Auto DevOps)
- Product hints (you want to use)
- Product interface (where you are in the process)
- Product analytics (ConvDev index)
- Analytics (warehouse and usage ping)

## Responsibilities

The COO ensures that we that we capturing relevant data, perform analysis, distill insights, and implementing actions based on them.

- Customer success
- Pricing
- Cross department definitions (what is a customer, subscription, sale stage, etc.)
- Data warehouse/data collection
- Analytics/data transformation
- [Company dashboard](https://about.gitlab.com/handbook/finance/analytics/)
- [Sales enablement](https://gitlab.com/gitlab-com/organization/issues/95)
- Cross functional automation (marketing, sales, billing)
- Scaling the company as it grows

The analytics effort is cross-functional:

- Product => what features do people want to pay for?
- Engineering => how do we instrument our product and services?
- Product marketing => what is the most effective message?
- Lead generation => what channels are most effect?
- Initial sale => what free users are most likely to buy?
- Customer Success => who do we message what to increase adoption?

## Reports

- Director of Customer Success
- Analytics team
- VP of Scaling
- Maybe Sales Operations
- Maybe Support

## Requirements

* Experience successfully leading a comparable effort
* Result focus
* Demonstrated use of analytics to double down on successful efforts
* Outstanding written communication skills
* Desire to develop a team
* Ability to partner with other executive team members
* Successful completion of a [background check](/handbook/people-operations/#background-checks).

## To Apply

We're not recruiting yet for this position.
