---
layout: markdown_page
title: "GitLab Primer"
---

Read the pages below to learn more about GitLab:

1. [About](https://about.gitlab.com/about/)
1. [Strategy](https://about.gitlab.com/strategy/#sequence)
1. [Direction](https://about.gitlab.com/direction/#scope)
1. [Handbook](https://about.gitlab.com/handbook/)
1. [Team](https://about.gitlab.com/team/)
1. [Features](https://about.gitlab.com/features/)
1. [Products](https://about.gitlab.com/products/)
1. [Values](https://about.gitlab.com/handbook/values/)
1. [Culture](https://about.gitlab.com/culture/)
1. [OKRs](https://about.gitlab.com/okrs/)
1. [2/3 market share](https://about.gitlab.com/2017/06/29/whats-next-for-gitlab-ci/)
